module Main(main) where

import SOE

import Raster
import System.IO
import Constants

---------------------------------------------------------------

main :: IO ()
main = 
   do cr <- getData c_snow_bunny_raster_result_path
      putStrLn "Calculate minimal, maximal and total Area. The result will be saved to /result/min_max_total.txt." 
      putStrLn "This window will close automatically when finished. Please wait a moment..."
      calcMinMaxTotalArea 4.0 cr 20
      return ()

---------------------------------------------------------------

type CoordRaster attr = (Int,Int,Int,Int,Int,attr, Raster (Int,Int) attr)

nc (c,r,x,y,s,v,rr) = c
nr (c,r,x,y,s,v,rr) = r
xl (c,r,x,y,s,v,rr) = x
yl (c,r,x,y,s,v,rr) = y
cs (c,r,x,y,s,v,rr) = s
nv (c,r,x,y,s,v,rr) = v
rs (c,r,x,y,s,v,rr) = rr

---------------------------------------------------------------

calcMinMaxTotalArea val cr size = 
   do let areas = (calcAreas val cr)
      let output = ("Minimal Area: " ++ (show (size * (length (calcMinArea (head areas) areas)))) ++ "\n" ++ "Maximal Area: " ++ (show (size * (length (calcMaxArea (head areas) areas)))) ++ "\n" ++ "Total Area: " ++ (show (size * (calcTotalAreaSize areas))))
      writeFile "./result/min_max_total.txt" output
      putStrLn output
      return ()

calcAreas :: Double -> CoordRaster Double -> [[((Int, Int), Double)]]
calcAreas val cr = calcAreasAkk val cr (pairs (rs cr)) [] where
   calcAreasAkk val cr [] result = result
   calcAreasAkk val cr (((x, y), v) : xs) result = {-trace ("result -" ++ (show(length(result)))) $-}
      if (containsNotElem ((x, y), v) result && val <= v) then calcAreasAkk val cr xs ((calcArea val cr ((x, y), v) (pairs(rs cr))) : result)
      else calcAreasAkk val cr xs result

containsNotElem :: ((Int, Int), Double) -> [[((Int, Int), Double)]] -> Bool
containsNotElem x [] = True 
containsNotElem x (xs : xss) = if elem x xs then False else containsNotElem x xss

calcArea :: Double -> CoordRaster Double -> ((Int, Int), Double) -> [((Int, Int), Double)] -> [((Int, Int), Double)]
calcArea val cr p list = calcAreaAkk val cr (p : []) list (p : []) where
   calcAreaAkk val cr [] list result = result
   calcAreaAkk val cr (u : unchecked) list result = let newNeigh = calcNeighbors val cr u list result in calcAreaAkk val cr (unchecked ++ newNeigh) list (result ++ newNeigh)

calcNeighbors :: Double -> CoordRaster Double -> ((Int, Int), Double) -> [((Int, Int), Double)] -> [((Int, Int), Double)] ->  [((Int, Int), Double)]
calcNeighbors val cr p list result = calcNeighborsAkk val cr p list result [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)] where
   calcNeighborsAkk val cr ((x, y), v) list result [] = []
   calcNeighborsAkk val cr ((x, y), v) list result ((a, b) : rest) = checkNeighbor val cr ((x, y), v) (a, b) list result ++ calcNeighborsAkk val cr ((x, y), v) list result rest

checkNeighbor :: Double -> CoordRaster Double -> ((Int, Int), Double) -> (Int, Int) -> [((Int, Int), Double)] -> [((Int, Int), Double)] -> [((Int, Int), Double)]
checkNeighbor val cr ((x, y), v) (a, b) list result = if (isInIndex cr (x, y) (a, b)) then (checkNeighborBla val cr (list!!((x+a)*(nc cr)+(y+b))) (x, y) list result) else [] -- if (checkNeighborBla val cr (list!!(a*(nc cr)+b)) (a, b) result) then [(list!!(a*(nc cr)+b))]  else []

checkNeighborBla :: Double -> CoordRaster Double -> ((Int, Int), Double) -> (Int, Int) -> [((Int, Int), Double)] -> [((Int, Int), Double)] -> [((Int, Int), Double)]
checkNeighborBla val cr ((x, y), v) (a, b) list result = if ((val <= v) && not(elem ((x, y), v) result)) then (((x, y), v) : []) else []

isInIndex :: CoordRaster Double -> (Int, Int) -> (Int, Int) -> Bool
isInIndex cr (x, y) (a, b) = x+a >= 0 && y+b >= 0 && x+a < (nr cr) && y+b < (nc cr)

--------------------------------------------------

calcMaxArea :: [((Int, Int), Double)] -> [[((Int, Int), Double)]] -> [((Int, Int), Double)]
calcMaxArea a (x : []) = largerArea a x
calcMaxArea a (x : xs) = calcMaxArea (largerArea a x) xs

largerArea :: [((Int, Int), Double)] -> [((Int, Int), Double)] -> [((Int, Int), Double)]
largerArea a b =
   if(length a >= length b) then a
   else b

calcMinArea :: [((Int, Int), Double)] -> [[((Int, Int), Double)]] -> [((Int, Int), Double)]
calcMinArea a (x : []) = smallerArea a x
calcMinArea a (x : xs) = calcMinArea (smallerArea a x) xs

smallerArea :: [((Int, Int), Double)] -> [((Int, Int), Double)] -> [((Int, Int), Double)]
smallerArea a b =
   if(length a <= length b) then a
   else b

calcTotalAreaSize :: [[((Int, Int), Double)]] -> Int
calcTotalAreaSize [] = 0
calcTotalAreaSize (x : xs) = length x + calcTotalAreaSize xs

---------------------------------------------------------------

getData :: String -> IO (CoordRaster Double)
getData fname =
 do rawData  <- readFile fname
    let pieces  = words rawData
    let header  = take 12 pieces
    let ncols   = read (header !! 1) :: Int
    let nrows   = read (header !! 3) :: Int
    let xll     = read (header !! 5) :: Int
    let yll     = read (header !! 7) :: Int
    let csize   = read (header !! 9) :: Int
    let no_val  = read (header !! 11) :: Double
    let values  = map read (drop 12 pieces) :: [Double]
    let tuples  = [((x, y), values!!(x*ncols+y)) | x <- [0..nrows-1], y <- [0..ncols-1] ]
    return (ncols,nrows,xll,yll,csize,no_val, Raster tuples)