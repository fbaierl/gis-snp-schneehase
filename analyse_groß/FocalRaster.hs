module FocalRaster where

import Raster

class Rasters raster => FocalRasters raster where

  neighRaster :: Eq addr =>
                    (addr -> [addr]) -- neighbourhood function
                 -> raster addr attr -> raster addr [addr]
    -- raster recording for each address its neighbourhood

  neighRaster neigh r = makeRaster neigh (domain r)

  mapFocalRaster :: Eq addr =>
                        (addr -> [addr]) -- neighbourhood function
                     -> ([attr] -> attr) -- accumulation function
                     -> raster addr attr -> raster addr attr

  mapFocalRaster neigh acc r =
      mapRaster (acc.(map (attrVal r)))
                   (neighRaster neigh r)


instance FocalRasters Raster


