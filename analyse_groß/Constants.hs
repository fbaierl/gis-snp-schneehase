module Constants where

hutValue = -112123.0

c_window_size_x = 700 :: Int
c_window_size_y = 700 :: Int

c_data_path_huts = "./data/huetten.txt"
c_data_path_slope = "./data/slope.txt"
c_data_path_dtm = "./data/dtm20.txt"
c_data_path_veg = "./data/vegetation_classified.txt"

c_trails_folder_path = "./data/trails" 
c_borders_folder_path = "./data/borders" 

c_snow_bunny_raster_result_path = "./result/schneehase_result.txt"
c_snow_grouse_raster_result_path = "./result/schneehuhn_result.txt"
c_snow_bunny_grouse_conflict_raster_result_path = "./result/schneehase_schneehuhn_konflikt_result.txt"
c_snow_bunny_trails_hut_conflict_raster_result_path = "./result/schneehase_wanderwege_konflikt_result.txt"


