module Draw where

import Raster
import Constants
import VectorIO
import RasterIO
import SOE


-- Farbcode f�r die Bildpunkte
colorCode :: Double -> Color
colorCode value =
   if value == -1 then White
   else if value == hutValue then Magenta
   else if value == 0 then Black       
   else if value <= 1 then DarkKhaki       
   else if value <= 2 then YellowGreen      
   else if value <= 3 then ForestGreen
   else if value <= 3.9 then Green       
   else if value > 4 then DarkGreen         
   else Black  

colorCodeConflict :: Double -> Color
colorCodeConflict value = 
   if value == -1 then White
   else if value == 0 then Black
   else Red


---------------------------------------------------------------
-- Wanderwege darstellen

drawVectors :: Window -> [[(Double, Double, Double)]] -> Color -> Int -> Int -> IO ()
drawVectors w (x:xs) c xll yll = do processVector w x c xll yll
                                    drawVectors w xs c xll yll
                                    return() 

drawVectors w [] c _ _ = return()


-- einzelnen Wanderweg malen
processVector :: Window -> [(Double, Double, Double)] -> Color -> Int -> Int -> IO ()
processVector w (x:y:xs) c xll yll = do drawVector w x y c xll yll
                                        processVector w (y:xs) c xll yll
processVector w _ c _ _ = return()

drawVector :: Window -> (Double, Double, Double) -> (Double, Double, Double) -> Color -> Int -> Int -> IO ()
drawVector w (x1,y1,z1) (x2,y2,z2) c xll yll = do 
          windowSize <- getWindowSize w
          let windowSizeY = snd windowSize
          let px1 = round $ (x1 - fromIntegral xll) / 5
          let py1 = windowSizeY - (round $ (y1 - fromIntegral yll) / 5)
          let px2 = round $ (x2 - fromIntegral xll) / 5 
          let py2 = windowSizeY - (round $ (y2 - fromIntegral yll) / 5) 
          drawInWindow w (withColor c (line (px1, py1) (px2, py2)))
          return()


---------------------------------------------------------------

-- Tupel darstellen
-- Tupelaufbau: ((x-Koord., y-Koord.), Farbwert)

drawRaster w cr =
  let xll = xl cr
      yll = yl cr
   in processTuples w (pairs (rs cr)) xll yll 


processTuples w (((x,y),c):ts) xll yll =
      do drawInWindow w (withColor c (scale (x,y,xll,yll)))
         processTuples w ts xll yll

processTuples w [] _ _ = return()

---------------------------------------------------------------

-- Skalieren der Bildpunkte passend zu Raster und Fenstergr��e;
-- Bildpunkte werden als kleine Quadrate der Seitenl�nge lg gezeichnet

scale (x,y,xll,yll) =
  polygon [(u,v),(u+lg,v),(u+lg,v+lg),(u,v+lg)]
  where (v,u) =  ((x-xll) `div` 5, (y-yll) `div` 5)
        lg = 5


---------------------------------------------------------------
