module Main(main) where

import SOE

import Raster
import FocalRaster
import VectorIO
import RasterIO
import TabRast
import System.Directory(getDirectoryContents)
import System.IO
import Constants


main :: IO (Int)
main = 
   do  snowBunnyValueRaster <- loadCoordRasterData c_snow_bunny_raster_result_path
       snowGrouseValueRaster <- loadCoordRasterData c_snow_grouse_raster_result_path
       hutRaster   <- loadCoordRasterData c_data_path_huts
       
       let hutCoordRaster = mapCoordRaster hut_pred hutRaster
           
       let natureCoordRaster = zipWithCoordRaster calcNatureHabitateValue snowBunnyValueRaster snowGrouseValueRaster 
       let valueRaster = zipWithCoordRaster calcHabitateValue natureCoordRaster hutCoordRaster     
     
       window <- openWindow "Snowbunny/Snow Grouse conflict zones" (600,300)
       
       drawInWindow window (text (40,40) "Press any key to start calucations ...")
       drawInWindow window (text (60,70) "... this window will close automatically when finished.")
       myGetKey window

       -- todo safe the result raster
       saveCoordRasterData c_snow_bunny_grouse_conflict_raster_result_path valueRaster

       -- myGetKey window    
       closeWindow window
        
       return 0
      

---------------------------------------------------------------

hut_pred h =
  if h >= 0 then 1
  else 0

---------------------------------------------------------------

calcHabitateValue :: Double -> Double -> Double
calcHabitateValue natureValue isHut =
  if isHut == 1 then hutValue
  else natureValue

calcNatureHabitateValue :: Double -> Double -> Double
calcNatureHabitateValue bunny grouse = 
   if bunny == -1 || grouse == -1 then -1            -- no data
   else if bunny < 2 || grouse < 2 then 0            -- no conflict // todo: < 2 good measure threshold?
   else 1                                            -- conflict


-- Abwarten der Benutzereingabe
myGetKey w
   = do k <- maybeGetWindowEvent w
        case k of
          Just (Key c d) -> return c
          _ -> myGetKey w

