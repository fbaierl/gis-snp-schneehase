module Main(main) where

import SOE

import Raster
import FocalRaster
import VectorIO
import RasterIO
import TabRast
import System.Directory(getDirectoryContents)
import System.IO
import Constants


main :: IO (Int)
main = 
   do  snowBunnyValueRaster <- loadCoordRasterData c_snow_bunny_raster_result_path
       hutRaster   <- loadCoordRasterData c_data_path_huts
       trailData <- loadVectorData c_trails_folder_path
       
       let hutCoordRaster = mapCoordRaster hut_pred hutRaster
           
       window <- openWindow "Snowbunny Habitat - Trail/Hut distance" (600,300)
       
       drawInWindow window (text (40,40) "Press any key to start calucations ...")
       drawInWindow window (text (60,70) "... this window will close automatically when finished.")
       myGetKey window

       -- calculate distance raster to huts & trails seperatly
       let distanceToHutRaster = createDistanceToHutsRaster hutRaster
       let distanceToTrailsRaster = createDistanceToTrailsRaster snowBunnyValueRaster $ turnVectorList (xl hutRaster) (yl hutRaster) (nc hutRaster * cs hutRaster) trailData

       let distanceRaster = zipWithCoordRaster min distanceToTrailsRaster distanceToHutRaster
       let resultRaster = zipWithCoordRaster zipWithHabitateFunction distanceRaster snowBunnyValueRaster
       
       -- safe the result raster
       saveCoordRasterData c_snow_bunny_trails_hut_conflict_raster_result_path resultRaster

         
       closeWindow window
        
       return 0
      

---------------------------------------------------------------

zipWithHabitateFunction :: Double -> Double -> Double
zipWithHabitateFunction distanceVal habitatVal =
                                      if habitatVal == -1 then -1   -- no data
                                      else if habitatVal < 4 then 0 -- not a habitat
                                      else distanceVal

---------------------------------------------------------------

hut_pred h =
  if h >= 0 then 1
  else 0

---------------------------------------------------------------

get1st (a,_,_) = a
get2nd (_,b,_) = b
get3rd (_,_,c) = c

---------------------------------------------------------------
---------------------------------------------------------------
-- Huts -------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------


createDistanceToHutsRaster :: (CoordRaster Double) -> (CoordRaster Double)
createDistanceToHutsRaster r = (nc r, nr r, xl r, yl r, cs r, nv r, Raster (calcMinimalDistanceListToHut (pairs (rs r)) r))

calcMinimalDistanceListToHut :: [((Int, Int), Double)] -> (CoordRaster Double) -> [((Int, Int), Double)]
calcMinimalDistanceListToHut x hutRaster = map (calcMinimalDistanceToHuts hutRaster) x

calcMinimalDistanceToHuts :: (CoordRaster Double) -> ((Int, Int), Double) -> ((Int, Int), Double)
calcMinimalDistanceToHuts hutRaster ((x, y), v) = let huts = findHuts hutRaster
                                                      minToHut = calcMinimalDistancePointToHuts (x,y) huts
                                                  in ((x, y), minToHut)

----------------------------------------------------------------------
-- finds all huts in the raster and saves the coordinates in a list --
----------------------------------------------------------------------
findHuts :: (CoordRaster Double) -> [(Double, Double)]
findHuts raster = [(fromIntegral x,fromIntegral y) | ((x,y),v) <- filter (isHut $ nv raster) (pairs (rs raster)) ]
                     where isHut nv ((x,y),v) = v /= nv 

----------------------------------------------------------------------
-- calculates the minimal distance of one point to the closest hut --
----------------------------------------------------------------------
calcMinimalDistancePointToHuts :: (Int, Int) -> [(Double, Double)] -> Double
calcMinimalDistancePointToHuts (x,y) (h:hs) = 
                        let currentHut = calcMinimalDistanceToHut (x,y) h
                            otherHut = calcMinimalDistancePointToHuts (x,y) hs
                        in min currentHut otherHut
calcMinimalDistancePointToHuts (x,y) [] = 9999999999999.0 -- some big number

---------------------------------------------------------------------
-- calculates the minimal distance of one point to one hut (point) --
-------------------------------------------------------------
calcMinimalDistanceToHut :: (Int, Int) -> (Double, Double) -> Double
calcMinimalDistanceToHut (x1 , y1) (x2 , y2) = sqrt (x'*x' + y'*y')
                        where
                           x' = fromIntegral x1 - x2
                           y' = fromIntegral y1 - y2



---------------------------------------------------------------
---------------------------------------------------------------
-- Trails -----------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

createDistanceToTrailsRaster :: (CoordRaster Double) -> [[(Double, Double, Double)]] -> (CoordRaster Double)
createDistanceToTrailsRaster b trails = (nc b, nr b, xl b, yl b, cs b, nv b, Raster (calcMinimalDistanceListToTrails (pairs (rs b)) trails))

calcMinimalDistanceListToTrails :: [((Int, Int), Double)] -> [[(Double, Double, Double)]] -> [((Int, Int), Double)]
calcMinimalDistanceListToTrails x trailData = map (calcMinimalDistanceToTrails trailData) x

calcMinimalDistanceToTrails :: [[(Double, Double, Double)]] -> ((Int, Int), Double) -> ((Int, Int), Double)
calcMinimalDistanceToTrails trailData ((x, y), v) = let minToTrail = calcMinimalDistancePointToTrails (x, y) trailData
                                                    in ((x, y), minToTrail)

------------------------------------------------------------------------
-- calculates the minimal distance of one point the the closest trail --
------------------------------------------------------------------------
calcMinimalDistancePointToTrails :: (Int, Int) -> [[(Double, Double, Double)]] -> Double
calcMinimalDistancePointToTrails coord (x:xs) = 
          let currentTrail = calcMinimalDistancePointToTrail coord x
              otherTrail   = calcMinimalDistancePointToTrails coord xs
          in min currentTrail otherTrail
calcMinimalDistancePointToTrails coord [] = 9999999999999.0 -- some big number

------------------------------------------------------------
-- calculates the minimal distance of one point ONE trail --
------------------------------------------------------------
calcMinimalDistancePointToTrail :: (Int, Int) -> [(Double, Double, Double)] -> Double
calcMinimalDistancePointToTrail (x,y) (u:v:us) = 
          let currentDistance = calcDstLineSegmentPoint (get1st u, get2nd u) (get1st v, get2nd v) (fromIntegral x, fromIntegral y)
              otherDistance   = calcMinimalDistancePointToTrail (x,y) (v:us)
          in min currentDistance otherDistance 
calcMinimalDistancePointToTrail _ [] = 9999999999999.0 -- some big number
calcMinimalDistancePointToTrail _ [x] = 9999999999999.0 -- list with one element


-----------------------------------------------------------------
-- Return minimum distance between line segment vw and point p --
-----------------------------------------------------------------
calcDstLineSegmentPoint :: (Double, Double) -> (Double, Double) -> (Double, Double) -> Double
calcDstLineSegmentPoint (v1, v2) (w1, w2) (p1, p2) = 
                let dx = w1 - v1
                    dy = w2 - v2  
                    t = ((p1 - v1) * dx + (p2 - v2) * dy) / (dx * dx + dy * dy) 
                    closest | t < 0     = (v1, v2)
                            | t > 0     = (w1, w2)
                            | otherwise = (v1 + t * dx, v2 + t * dy)
                    dx2 | t < 0     = p1 - v1
                        | t > 0     = p1 - w1
                        | otherwise = p1 - (fst closest)
                    dy2 | t < 0     = p2 - v2
                        | t > 0     = p2 - w2
                        | otherwise = p2 - (snd closest)
                in sqrt (dx2 * dx2 + dy2 * dy2)


--------------------------------------------------------------------------------
-- calculates the distance of one line defined by two points to another point --
-- https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line              --
-- (x1, y1), (x2, y2) = line, (x0,y0) = point                                  --
--------------------------------------------------------------------------------
calcDistanceVectorToPoint :: (Double, Double) -> (Double, Double) -> (Double, Double) -> Double
calcDistanceVectorToPoint (x1, y1) (x2, y2) (x0, y0) = (abs ((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1)) / (sqrt (((**) (y2 - y1) 2) + ((**) (x2 - x1) 2)))



turnVectorList :: Int -> Int -> Int -> [[(Double, Double, Double)]] -> [[(Double, Double, Double)]]
turnVectorList xl yl width (x : xs) = (turnVector xl yl width x) : (turnVectorList xl yl width xs)
turnVectorList xl yl width [] = []

turnVector :: Int -> Int -> Int -> [(Double, Double, Double)] -> [(Double, Double, Double)]
turnVector xl yl width (x : xs) = (turnPoint xl yl width x) : (turnVector xl yl width xs)
turnVector xl yl width [] = []

turnPoint :: Int -> Int -> Int -> (Double, Double, Double) -> (Double, Double, Double)
turnPoint xl yl width (x, y, z) = (((fromIntegral width) + (fromIntegral xl) + (fromIntegral yl)) - y, ((fromIntegral yl)-(fromIntegral xl)) + x, z)
--turnPoint xl yl width (x, y, z) = (((fromIntegral xl) - (fromIntegral yl)) + y, ((fromIntegral width)+(fromIntegral yl)+(fromIntegral xl)) - x, z)



---------------------------------------------------------------

-- Abwarten der Benutzereingabe
myGetKey w
   = do k <- maybeGetWindowEvent w
        case k of
          Just (Key c d) -> return c
          _ -> myGetKey w
