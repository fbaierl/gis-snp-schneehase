module VectorIO where

import System.Directory(getDirectoryContents)
import System.IO
import TenaryOperator

type Vectors3D = [[(Double, Double, Double)]] 

loadVectorData :: String -> IO (Vectors3D)
loadVectorData folderName =
   do fileList <- getDirectoryContents folderName
      let filteredFileList = filter (/= ".") $ filter (/= "..") fileList
      vectorData <- readVectorFiles filteredFileList folderName
      return vectorData

readVectorFiles :: [String] -> String -> IO [[(Double, Double, Double)]]
readVectorFiles (f:fs) folderName = do
                first <- readVectorFile (folderName ++ "/" ++ f)
                rest  <- readVectorFiles fs folderName
                return ([first] ++ rest)
readVectorFiles [] _ = do 
                return []

readVectorFile :: FilePath -> IO [(Double, Double, Double)]
readVectorFile fname = do input <- readFile fname
                          return $ readVectorLines (lines input)

readVectorLines :: [String] -> [(Double, Double, Double)]
readVectorLines (x:xs) = do let pieces  = words x
                            let coordinates  = take 3 pieces
                            let x   = read (coordinates !! 0) :: Double
                            let y   = read (coordinates !! 1) :: Double
                            let z   = read (coordinates !! 2) :: Double
                            return (x,y,z) ++ readVectorLines xs
readVectorLines [] = []
