module Karte where

import SOE

main = runGraphics $
  do w <- openWindow "Meine erste Landkarte" (300,300)
     drawInWindow w 
       (withColor Green 
        (polygon [(100, 50),
                  ( 75,100),
                  (150,100)]))
     drawInWindow w 
       (withColor Red 
        (polygon [( 75,100),
                  (125,175),
                  ( 50,200),
                  ( 25,150)]))
     drawInWindow w 
       (withColor Yellow 
        (polygon [( 75,100),
                  (150,100),
                  (125,125),
                  (150,150),
                  (175,150),
                  (125,175)]))
     drawInWindow w 
       (withColor Blue 
        (polygon [(150,100),
                  (200,125),
                  (175,150),
                  (150,150),
                  (125,125)])) 
     getKey w
     closeWindow w


