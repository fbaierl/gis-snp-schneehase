module Main(main) where

import SOE

import Raster
import FocalRaster
import VectorIO
import RasterIO
import TabRast
import System.Directory(getDirectoryContents)
import System.IO
import Constants


main :: IO (Int)
main = 
   do  slopeRaster <- loadCoordRasterData c_data_path_slope
       altRaster   <- loadCoordRasterData c_data_path_dtm
       vegRaster   <- loadCoordRasterData c_data_path_veg
       hutRaster   <- loadCoordRasterData c_data_path_huts

       let slopeCoordRaster = mapCoordRaster slope_pred slopeRaster
       let altCoordRaster = mapCoordRaster alt_pred altRaster
       let vegCoordRaster = mapCoordRaster veg_pred vegRaster
       let hutCoordRaster = mapCoordRaster hut_pred hutRaster
           
       let natureCoordRaster = zipWith3CoordRaster calcNatureHabitateValue slopeCoordRaster altCoordRaster vegCoordRaster 
       let valueRaster = zipWithCoordRaster calcHabitateValue natureCoordRaster hutCoordRaster     
     
       window <- openWindow "SnowGrouse - Habitat" (600,300)
       
       drawInWindow window (text (40,40) "Press any key to start calucations ...")
       drawInWindow window (text (60,70) "... this window will close automatically when finished.")
       myGetKey window

       -- safe the result raster
       saveCoordRasterData c_snow_grouse_raster_result_path valueRaster
   
       closeWindow window
        
       return 0
      

slope_pred, alt_pred, veg_pred :: Double -> Double

veg_pred v = 
   if v == -9999 then -1.0
   else 3

slope_pred s =
   if s == -9999 then -1.0
   else if s >= 0  && s <= 50 then 1.0
   else 0
       
alt_pred a =
   if a == -9999 then -1.0
   else if 1800.0 <= a && a <= 3000.0 then 1.0
   else 0

hut_pred h =
  if h >= 0 then 1
  else 0

---------------------------------------------------------------

calcHabitateValue :: Double -> Double -> Double
calcHabitateValue natureValue isHut =
  if isHut == 1 then hutValue
  else natureValue

calcNatureHabitateValue :: Double -> Double -> Double -> Double
calcNatureHabitateValue slope alt veg = 
   if slope == -1 || alt == -1 || veg == -1 then -1  -- no data
   else if slope == 0 || alt == 0 || veg == 0 then 0 -- not inhabitable 
   else 4.5                                          -- inhabitable 



-- Abwarten der Benutzereingabe
myGetKey w
   = do k <- maybeGetWindowEvent w
        case k of
          Just (Key c d) -> return c
          _ -> myGetKey w

