module Main(main) where

import SOE

import Raster
import FocalRaster
import VectorIO
import RasterIO
import TabRast
import System.Directory(getDirectoryContents)
import System.IO
import Constants
import Draw
import Constants



main :: IO ()
main = 
   do  resultRaster <- loadCoordRasterData c_snow_bunny_trails_hut_conflict_raster_result_path
       habitatWindow <- openWindow "Schneehase - Wanderwege Konflikt Zonen" (c_window_size_x, c_window_size_y)            
       
       drawRaster habitatWindow $ mapCoordRaster colorCodeTrailConflict resultRaster  

       trailData <- loadVectorData c_trails_folder_path   
       drawVectors habitatWindow trailData Magenta (xl resultRaster) (yl resultRaster)
  
       borderData <- loadVectorData c_borders_folder_path
       drawVectors habitatWindow borderData Yellow (xl resultRaster) (yl resultRaster)

       myGetKey habitatWindow    
       closeWindow habitatWindow                     



colorCodeTrailConflict :: Double -> Color
colorCodeTrailConflict value = 
   if value == -1 then White
   else if value == 0 then Black
   else if value < 100 then Red
   else if value < 250 then Orange
   else if value < 500 then Yellow
   else Green



myGetKey w
   = do k <- maybeGetWindowEvent w
        case k of
          Just (Key c d) -> return c
          _ -> myGetKey w
