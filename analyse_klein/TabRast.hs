module TabRast where

import Raster
import FocalRaster

------------------------------------------------------------------------------------------------

-- tabular representation of rasters

------------------------------------------------------------------------------------------------

tabToRast xss = [ ((i,j),(xss!!i)!!j) | i <- [0..length xss - 1], j <- [0.. length (xss!!i) - 1] ]

------------------------------------------------------------------------------------------------

makeRaster2 ps =  Raster ps

------------------------------------------------------------------------------------------------

group [] = []
group (((i,j),x):ps) = (((i,j),x):qs):group rs
 where (qs,rs) = takeDropWhile (\((k,_),_) -> i==k) ps

takeDropWhile :: (a -> Bool) -> [a] -> ([a],[a])

takeDropWhile p [] = ([],[])
{-
takedropWhile p (x:xs)
 | p x        = let (ys,zs) = takeDropWhile p xs in ((x:ys),zs)
 | otherwise = ([],(x:xs))
-}
takeDropWhile p xs =
 if p (head xs)
    then let (ys,zs) = takeDropWhile p (tail xs)
          in  ((head xs : ys),zs)
    else ([],xs)

------------------------------------------------------------------------------------------------

rastToTab ps = map (map snd) (group ps)

------------------------------------------------------------------------------------------------

sampleTable =
  [[4,2,5,4],
   [6,4,7,8],
   [5,8,2,3],
   [1,6,3,9]]

sampleRaster = makeRaster2 (tabToRast sampleTable)

------------------------------------------------------------------------------------------------

printTab ps = putStr (unlines (map show ps))

------------------------------------------------------------------------------------------------

-- concrete case of a neighbourhood function

------------------------------------------------------------------------------------------------

nb r (i,j)  =  intersect [ (i+k,j+l) | k <- [-1..1], l <- [-1..1] ]  (domain r)

intersect ps qs = [ p | p <- ps, p `elem` qs ]

------------------------------------------------------------------------------------------------

testRaster = mapFocalRaster (nb sampleRaster) sum sampleRaster
