module RasterIO where

import Raster
import System.Directory(getDirectoryContents)
import System.IO
import TenaryOperator


type CoordRaster attr = (Int,Int,Int,Int,Int,attr, Raster (Int,Int) attr)

nc (c,r,x,y,s,v,rr) = c
nr (c,r,x,y,s,v,rr) = r
xl (c,r,x,y,s,v,rr) = x
yl (c,r,x,y,s,v,rr) = y
cs (c,r,x,y,s,v,rr) = s
nv (c,r,x,y,s,v,rr) = v
rs (c,r,x,y,s,v,rr) = rr

---------------------------------------------------------------

mapCoordRaster f cr =
      ((nc cr),(nr cr),(xl cr),(yl cr),(cs cr),(nv cr), mapRaster f (rs cr))

--------------------------------------------------------------

zipWithCoordRaster f cr1 cr2
 | nc cr1 == nc cr2 && nr cr1 == nr cr2 &&
   xl cr1 == xl cr2 && yl cr1 == yl cr2 &&
   cs cr1 == cs cr2 && nv cr1 == nv cr2     =
      ((nc cr1),(nr cr1),(xl cr1),(yl cr1),(cs cr1),(nv cr1),
        zipWithRaster f (rs cr1) (rs cr2))

zipWith3CoordRaster f cr1 cr2 cr3
 | nc cr1 == nc cr2 && nr cr1 == nr cr2 &&
   xl cr1 == xl cr2 && yl cr1 == yl cr2 &&
   cs cr1 == cs cr2 && nv cr1 == nv cr2 &&
   nc cr2 == nc cr3 && nr cr2 == nr cr3 &&
   xl cr2 == xl cr3 && yl cr2 == yl cr3 &&
   cs cr2 == cs cr3 && nv cr2 == nv cr3 =
      ((nc cr1),(nr cr1),(xl cr1),(yl cr1),(cs cr1),(nv cr1),
        zipWith3Raster f (rs cr1) (rs cr2) (rs cr3))

--------------------------------------------------------------

loadCoordRasterData :: String -> IO (CoordRaster Double)
loadCoordRasterData fname =
 do rawData  <- readFile fname
    let pieces  = words rawData
    let header  = take 12 pieces
    let ncols   = read (header !! 1) :: Int
    let nrows   = read (header !! 3) :: Int
    let xll     = read (header !! 5) :: Int
    let yll     = read (header !! 7) :: Int
    let csize   = read (header !! 9) :: Int
    let no_val  = read (header !! 11) :: Double
    let values  = map read (drop 12 pieces) :: [Double]
    let tuples  = [((x*csize+xll, y*csize+yll), values!!(x*ncols+y)) | x <- [0..nrows-1], y <- [0..ncols-1] ]
    return (ncols,nrows,xll,yll,csize,no_val, Raster tuples)

--------------------------------------------------------------

saveCoordRasterData :: String -> CoordRaster Double -> IO ()
saveCoordRasterData fpath dtmData = 
   do let header = "ncols         " ++ show (nc dtmData) ++ "\n" ++
                   "nrows         " ++ show (nr dtmData) ++ "\n" ++
                   "xllcorner     " ++ show (xl dtmData) ++ "\n" ++
                   "yllcorner     " ++ show (yl dtmData) ++ "\n" ++
                   "cellsize      " ++ show (cs dtmData) ++ "\n" ++
                   "NODATA_value  " ++ show (nv dtmData)
      let content = writeRasterInString (values (rs dtmData)) (nc dtmData) (nr dtmData)
      writeFile fpath $ header ++ "\n" ++ content


writeRasterInString :: [Double] -> Int -> Int -> String 
writeRasterInString values ncols nrows = 
                         do if nrows == 0 then ""
                            else writeRasterInString values ncols (nrows-1) ++ (nrows-1 == 0 ? "" :? "\n")  ++ (writeLineInString values ncols nrows ncols)

-- n is the horizontal number of the current value; starts at the right side (starts with ncols)
writeLineInString :: [Double] -> Int -> Int -> Int -> String
writeLineInString values ncols nrows n = 
                         do if n == 0 then ""
                            else (writeLineInString values ncols nrows (n-1)) ++ (ncols-1 == 0 ? "" :? " ") ++ (show (values !! (((nrows-1)*ncols) + n - 1)))


--------------------------------------------------------------
-- method for testing
test = do testR <- loadCoordRasterData "./data/test.txt"
          saveCoordRasterData "./test2.txt" testR
          return()





   
