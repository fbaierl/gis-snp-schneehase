getData :: String -> IO [(Int,Int,Double)]
getData filename =
    do rawData  <- readFile filename
        let pieces  = words rawData
            header  = take 12 pieces
            ncols   = read (header !! 1) :: Int
            nrows   = read (header !! 3) :: Int
            xll     = read (header !! 5) :: Int
            yll     = read (header !! 7) :: Int
            csize   = read (header !! 9) :: Int
            no_val  = read (header !! 11) :: Double
            values  = map read (drop 12 pieces) :: [Double]
            triples = [(x*csize+xll, y*csize+yll, values!!(x*ncols+y)) |
                            x <- [0..nrows-1], y <- [0..ncols-1] ]
        return triples