module Raster where

class Rasters raster where

  makeRaster :: (addr -> attr) -> [addr] -> raster addr attr

  domain :: Eq addr => raster addr attr -> [addr]

  attrVal :: Eq addr => raster addr attr -> addr -> attr

  mapRaster :: (attr1 -> attr2) -> raster addr attr1 -> raster addr attr2

  zipWithRaster :: Eq addr => (attr1 -> attr2 -> attr3) ->
       raster addr attr1 -> raster addr attr2 -> raster addr attr3

  zipWith3Raster :: Eq addr => (attr1 -> attr2 -> attr3 -> attr4) ->
       raster addr attr1 -> raster addr attr2 -> raster addr attr3 -> raster addr attr4

  addRaster, subtractRaster :: (Eq addr, Num attr) => 
       raster addr attr -> raster addr attr -> raster addr attr   

  addRaster r1 r2 = zipWithRaster (+) r1 r2

  subtractRaster r1 r2 = zipWithRaster (-) r1 r2

  values :: (Eq addr, Rasters raster) => raster addr attr -> [attr]

  foldrRaster :: (Eq addr, Rasters raster) => (attr -> b -> b) -> b -> raster addr attr -> b
  
  filterRaster :: (Eq addr, Rasters raster) => (addr -> Bool) -> raster addr attr -> Raster addr attr    -- filters via addr
  filterRasterAtr :: (Eq addr, Rasters raster) => (attr -> Bool) -> raster addr attr -> Raster addr attr -- filters via attr
  

{- Now we define a concrete data type  Raster  and make it into an instance of the type class Rasters. -}

data Raster addr attr = Raster [( addr , attr )]

dom ps = map fst ps
pairs (Raster ps) = ps

instance Rasters Raster where

  makeRaster f xs = Raster [(x,f x) | x <- xs]  -- f: Zuweisungsfunktion; xs: Addressenliste

  domain (Raster ps) = dom ps

  attrVal (Raster ps) x = lookup x ps
      where lookup x ((y,v):pp) = 
                 if x == y then v else lookup x pp
  -- works only for x `elem` domain (Raster ps)

  mapRaster f (Raster ps) = Raster (map g ps)
    where g (x,v) = (x,f v)

  zipWithRaster f (Raster ps) (Raster qs)
     | (dom ps) == (dom qs)   = 
         Raster (zipWith g ps qs)
           where g (x,u) (y,v) = (x,f u v) 

  zipWith3Raster f (Raster ps) (Raster qs) (Raster ts)
     | (dom ps) == (dom qs) && (dom qs) == (dom ts)   = 
         Raster (zipWith3 g ps qs ts)
           where g (x,u) (y,v) (z,w) = (x,f u v w) 

  values r = [ attrVal r x | x <- domain r ]
  
  foldrRaster f x r = foldr f x (values r)

  filterRaster p r = makeRaster (attrVal r) (filter p (domain r))
 
  filterRasterAtr p r = makeRaster (attrVal r) ([addr | addr <- (domain r), p ((attrVal r) addr) == True])

----------------------------------------------

instance (Show addr, Show attr) => Show (Raster addr attr) where
   show (Raster ps) = show ps

---------------------------------

r1, r2 :: Raster Int Int
r1 = Raster [(1,2),(2,4)]
r2 = Raster [(1,3),(2,7)]

rb :: Raster Integer Bool
rb = makeRaster odd [1..7]










